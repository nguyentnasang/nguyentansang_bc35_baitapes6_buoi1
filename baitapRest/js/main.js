function tinhDiemTrungBinh(...input) {
  let tong = 0;
  input.forEach((so) => (tong += so));
  let diem = tong / input.length;
  return diem.toFixed(1);
}

document.getElementById("btnKhoi1").onclick = () => {
  let Toan = document.getElementById("inpToan").value * 1;
  let Ly = document.getElementById("inpLy").value * 1;
  let Hoa = document.getElementById("inpHoa").value * 1;

  let KQKhoi1 = tinhDiemTrungBinh(Toan, Ly, Hoa);
  document.getElementById("tbKhoi1").innerText = KQKhoi1;
};

document.getElementById("btnKhoi2").onclick = () => {
  let Van = document.getElementById("inpVan").value * 1;
  let Su = document.getElementById("inpSu").value * 1;
  let Dia = document.getElementById("inpDia").value * 1;
  let TiengAnh = document.getElementById("inpEnglish").value * 1;

  let KQKhoi2 = tinhDiemTrungBinh(Van, Su, Dia, TiengAnh);
  document.getElementById("tbKhoi2").innerText = KQKhoi2;
};
