let DanhSachMau = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let contentHTML = "";

DanhSachMau.map((color, index) => {
  contentHTML += `<button   onclick='ThayDoiMau("${DanhSachMau[index]}")' class="color-button ${color}"></button>`;
  return DanhSachMau[index];
});
document.getElementById("colorContainer").innerHTML = contentHTML;

let luuClass = "";
let ThayDoiMau = (color) => {
  if (luuClass != "") {
    document.getElementById("house").classList.remove(luuClass);
  }
  luuClass = color;

  document.getElementById("house").classList.add(color);
};
