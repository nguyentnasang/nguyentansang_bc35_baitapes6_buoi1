// lấy nội dung từ thẻ heading
let noiDung = document.querySelector(".heading").textContent;

// Sử dụng Spread Operator để tách từng ký tự của text “Hover Me!”
let tachNoidung = [...noiDung];

// Tạo các thẻ span chứa các ký tự chữ sau khi đã tách được
let noiDungTheSpan = tachNoidung.map((str) => `<span>${str}</span>`);

// thêm các thẻ span đó vào thẻ heading
document.querySelector(".heading").innerHTML = noiDungTheSpan.join("");
